CREATE DATABASE Practica2017;
USE Practica2017;

/*
LAS INSTRUCCIONES ESTAN EN TODO EL DOCUMENTO DDL.
1. NO SE DEBE DE ALTERAR LA ESTRUCTURA DE LA BASE DE DATOS DADA.
2. CREAR EL ARCHIVO CORRESPONDIENTE DE DML PARA LA BASE DE DATOS.
3. EN EL ARCHIVO DML UTILIZAR TODOS LOS PROCEDIMIENTOS DECLARADOS EN EL ARCHIVO DDL.
4. SOLO DE DEBE DE CREAR LOS PROCEDIMIENTOS DE ALMACENADO INDICADOS.
*/


CREATE TABLE Tarea(
    idTarea INT NOT NULL AUTO_INCREMENT,
    titulo VARCHAR(30) NOT NULL,
    descripcion VARCHAR(40) NOT NULL,
	fecha_registro DATETIME NOT NULL,
	fecha_final DATETIME NOT NULL,
    PRIMARY KEY (idTarea)
);

CREATE TABLE Usuario(
	idUsuario INT NOT NULL AUTO_INCREMENT,
    nick VARCHAR(30) NOT NULL,
    contrasena VARCHAR(30) NOT NULL,
    cambios_contrasena INT NOT NULL,
	fecha_registro DATETIME NOT NULL,
    fecha_modificacion DATETIME NULL,
	picture TEXT NULL,
    PRIMARY KEY (idUsuario)
);

CREATE TABLE UsuarioTareas(
	idUsuarioTareas INT NOT NULL AUTO_INCREMENT,
    idUsuario INT NOT NULL,
    idTarea INT NOT NULL,
    PRIMARY KEY (idUsuarioTareas),
    FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario),
    FOREIGN KEY (idTarea) REFERENCES Tarea(idTarea)
);

/*PROCEDIMIENTOS ALMACENADOS PARA USUARIO*/

DROP PROCEDURE sp_insertUsuario;
DELIMITER $$ 
CREATE PROCEDURE sp_insertUsuario(
_nick VARCHAR(30),
_contrasena VARCHAR(30))
BEGIN
	INSERT INTO Usuario(nick, contrasena, cambios_contrasena, fecha_registro, fecha_modificacion, picture)
    VALUES(_nick, _contrasena, 0, NOW(), NOW(), NULL);
END $$
DELIMITER ;

/*
SP USUARIO UPDATE
CREAR UN PROCEDIMIENTO DE ALMACENADO QUE REALICE LO SIGUIENTE:
1. Modificar el usuario que espere los siguientes parametros:
a. nick
b. contrasena
c. picture
d. idUsuario
2. si se modifica la contraseña llevar el control de los cambios de contraseña
3. actualizar la fecha de modificacion
*/
DROP PROCEDURE sp_updateUsuario;
DELIMITER $$ 
CREATE PROCEDURE sp_updateUsuario
(IN _nick VARCHAR (30), 
IN _contrasena VARCHAR (30), 
IN _picture TEXT, 
IN _idUsuario INT)
BEGIN
	DECLARE _contrasenaNueva VARCHAR(30);
    DECLARE _numero INT;
    
    SET _contrasenaNueva = (SELECT Usuario.contrasena FROM Usuario.idUsuario = _idUsuario);
    
    IF (_contrasenaNueva = contrasena) THEN
		
        SET _numero = (SELECT Usuario.cambios_contrasena 
        FROM Usuario 
        WHERE Usuario.idUsuario = _idUsuario);
        
        SET _numero = _numero + 1;
        
		UPDATE Usuario SET Usuario.cambios_contrasena = _numero 
        WHERE Usuario.idUsuario = _idUsuario;
    
    END IF;
    
    UPDATE Usuario SET Usuario.nick = _nick, Usuario.contrasena = _contrasena, 
					Usuario.picture = _picture, Usuario.fecha_modificacion = NOW()
                    WHERE Usuario.idUsuario = _idUsuario;
END $$


/*
SP_AUTENTICAR
*/
DROP PROCEDURE sp_autenticarUsuario;
DELIMITER $$ 
CREATE PROCEDURE sp_autenticarUsuario(
_nick VARCHAR(30),
_contrasena VARCHAR(30))
BEGIN
	SELECT * FROM Usuario WHERE nick = _nick AND contrasena = _contrasena LIMIT 1;
END $$
DELIMITER ;

/*
SP USUARIO SELECT
CREAR UN PROCEDIMIENTO DE ALMACENADO QUE REALICE LO SIGUIENTE:
1. mostrar todos los usuarios con sus datos
2. la consulta debe de llevar lo siguiente:
a. idUsuario,
b. nick
c. contrasena
d. cambios_contrasena
e. fecha_registro
f. hora_registro
g. fecha_modificacion
h. hora_modificacion
i. picture
3. Debe de estar ordenada de los usuarios que han sido modificados ultimamente.
*/

DELIMITER $$
CREATE PROCEDURE sp_selectUsuario
(IN _idUsuario INT)
BEGIN
	SELECT Usuario.idUsuario, 
		   Usuario.nick, 
           Usuario.contrasena, 
           Usuario.cambios_contrasena,
    DATE_FORMAT(Usuario.fecha_registro, '%d-%c-%y') AS 'fecha_registro',
    DATE_FORMAT(Usuario.fecha_registro, '%h:%i:%s') AS 'hora_registro',
    DATE_FORMAT(Usuario.fecha_modificacion, '%d-%c-%y') AS 'fecha_modificacion',
    DATE_FORMAT(Usuario.fecha_modificacion, '%h:%i:%s') AS 'hora_modificacion',
    Usuario.picture 
    FROM Usuario 
    ORDER BY Usuario.fecha_modificacion ASC;
END
$$

/*
SP USUARIO DELETE
CREAR UN PROCEDIMIENTO DE ALMACENADO QUE REALICE LO SIGUIENTE:
1. Eliminar el usuario
2. Eliminar todas las tareas que tenga asignada
*/
DROP PROCEDURE sp_deleteUsuario;
DELIMITER $$
CREATE PROCEDURE sp_deleteUsuario(IN _idUsuario INT)
BEGIN
	DELETE FROM UsuarioTareas WHERE UsuarioTareas.idUsuario = _idUsuario;
    
    DELETE FROM Usuario WHERE Usuario.idUsuario = _idUsuario;
END $$


/*PROCEDIMIENTOS ALMACENADOS PARA TAREA*/

/*
SP TAREA INSERT
CREAR UN PROCEDIMIENTO DE ALMACENADO QUE REALICE LO SIGUIENTE:
1. Registrar una nueva tarea junto al usuario quien lo creo
*/
DROP PROCEDURE sp_insertTarea;
DELIMITER $$
CREATE PROCEDURE sp_insertTarea
(IN _titulo VARCHAR(30), IN _descripcion VARCHAR(40), IN _fecha DATETIME, IN _idUsuario INT)
BEGIN
	DECLARE _id INT;
    
	INSERT INTO Tarea(titulo, descripcion, fecha_registro, fecha_final) 
    VALUES (_titulo, _descripcion, NOW(), _fecha);
    
    SET _id = (SELECT MAX(Tarea.idTarea) FROM Tarea);
    
    INSERT INTO UsuarioTareas (idUsuario, idTarea) VALUES (_idUsuario, _id);
	/*SELECT MAX(Usuario.idUsuario) AS id FROM Usuario*/
END $$

CALL sp_insertUsuario ('Chavarrria', '1234')
/*
SP TAREA SELECT
CREAR UN PROCEDIMIENTO DE ALMACENADO QUE REALICE LO SIGUIENTE:
1. Mostrar todas las tareas del usuario por medio de su id
2. Mostrar en la consulta las siguientes columnas.
a. idTarea
b. titulo
c. descripcion
d. fecha_registro
e. hora del registro,
f. fecha_final,
g. hora final
3. la consulta debe de estar ordenada, por fecha y hora.
*/

DROP PROCEDURE sp_selectTarea;
DELIMITER $$
CREATE PROCEDURE sp_selectTarea
(IN _idUsuario INT)
BEGIN
SELECT Tarea.idTarea, 
	   Tarea.titulo, 
       Tarea.descripcion, 
DATE_FORMAT(Tarea.fecha_registro, '%d-%c-%y') AS 'fecha_registro',
DATE_FORMAT(Tarea.fecha_registro, '%h:%i:-%s') AS 'hora_registro',
DATE_FORMAT(Tarea.fecha_final, '%d-%c-%y') AS 'fecha_final',
DATE_FORMAT(Tarea.fecha_final, '%h:%i:-%s') AS 'hora_final'
FROM UsuarioTareas
    INNER JOIN Tarea ON UsuarioTareas.idTarea = Tarea.idTarea
    WHERE UsuarioTareas.idUsuario = _idUsuario
    ORDER BY Tarea.fecha_registro ASC;
END $$
