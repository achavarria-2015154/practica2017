var express = require('express');
var usuarioModel = require('../../model/usuario.model');
var usuarioRoute = express.Router();

usuarioRoute.route('/usuario/')
  .get(function(req, res) {
    usuario.selectAll(function(resultados) {
      if(resultados !== 0) {
        res.json(resultados);
      }else{
          res.json({"Mensaje":"No hay usuarios"});
      }
    });
  })
  .post(function(req, res) {
    var data = {
      nick: req.body.nick,
      contrasena: req.body.contrasena
    }

    usuario.insert(data, function(resultado){
      if(resultado.insertId > 0) {
        res.json({"Mensaje":"Se pudo guardar"});
      } else {
        res.json({"Mensaje":"No se pudo guardar"});
      }
    });
  });

usuarioRoute.route('/usuario/:idUsuario')
  .get(function(req, res) {
    var idUsuario = req.params.idUsuario;
    
    usuario.find(idUsuario, function(resultados){
      if(typeof resultados !== undefined) {
        res.json(resultados);
      } else {
        res.json({"Mensaje": "No se encontro el usuario"})
      }
    });
  })

  .put(function(req, res) {
    var idUsuario = req.params.idUsuario;

    var data = {
      idUsuario: req.body.idUsuario,
      nick: req.body.nick,
      contrasena: req.body.contrasena
    }

    if(idUsuario == data.idUsuario) {
      usuario.update(data, function(resultado){
          if(resultado && resultado.affectedRows)
        res.json(resultado);
      });
    }
  })
  .delete(function(req, res) {
    var idUsuario = req.params.idUsuario;
    usuario.delete(idUsuario, function(resultados){
      if(typeof resultados !== undefined) {
        res.json(resultados);
      } else {
        res.json({"Mensaje": "No se elimino el usuario"});
      }
    });
  });


module.exports = usuarioRoute;
