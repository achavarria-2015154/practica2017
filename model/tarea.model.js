var database = require('../config/database.config');
var tarea = {};

tarea.selectAll = function(callback) {
  if(database) {
    database.query('CALL sp_selectTarea',
    function(error, resultados) {
      if(error) throw error;
      if(resultados.length > 0) {
        callback(resultados);
      } else {
        callback(0);
      }
    })
  }
}

tarea.insert = function(data, callback) {
  if(database) {
    database.query("CALL sp_insertTarea(?,?,?,?)", [data.titulo, data.fecha, data.descripcion, data.idUsuario],
    function(error, resultado) {
      if(error) throw error;
      callback({"insertId": resultado.insertId});
    });
  }
}

tarea.update = function(data, callback) {
  if(database) {
    var sql = "CALL sp_updateTarea";
    database.query(sql,
    [data.titulo, data.fecha, data.descripcion, data.idUsuario, data.idTarea],
    function(error, resultado) {
      if(error) throw error;
      callback(resultado);
    });
  }
}

tarea.delete = function(idTarea, callback) {
  if(database) {
    var sql = "CALL sp_deleteTarea";
    database.query(sql, idTarea,
    function(error, resultado) {
      if(error) throw error;
      callback({"Mensaje": "Eliminado"});
    });
  }
}


module.exports = tarea;
